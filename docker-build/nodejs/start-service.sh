#!/bin/sh
#set -x

# Registry service bindings
if [ ! -z "$REGISTRY_HOSTNAME" ]; then
   export SVC_URI_REGISTRY="${REGISTRY_PROTOCOL}://${REGISTRY_USERNAME}:${REGISTRY_PASSWORD}@${REGISTRY_HOSTNAME}:${REGISTRY_PORT}/eureka/"
fi

# Elasticsearch service bindings
if [ ! -z "$ELASTICSEARCH_HOSTNAME" ]; then
   if [ ! -z "$ELASTICSEARCH_USERNAME" -a -z "$ES_CERT" ]; then
      export SVC_URI_ELASTICSEARCH="${ELASTICSEARCH_PROTOCOL}://${ELASTICSEARCH_USERNAME}:${ELASTICSEARCH_PASSWORD}@${ELASTICSEARCH_HOSTNAME}:${ELASTICSEARCH_PORT}"
      echo $SVC_URI_ELASTICSEARCH
   else
      export SVC_URI_ELASTICSEARCH="${ELASTICSEARCH_PROTOCOL}://${ELASTICSEARCH_HOSTNAME}:${ELASTICSEARCH_PORT}"
      echo $SVC_URI_ELASTICSEARCH
   fi
fi

# RabbitMQ service bindings
if [ ! -z "$RABBITMQ_HOSTNAME" ]; then
   export SVC_URI_RABBITMQ="${RABBITMQ_PROTOCOL}://${RABBITMQ_USERNAME}:${RABBITMQ_PASSWORD}@${RABBITMQ_HOSTNAME}:${RABBITMQ_PORT}/${RABBITMQ_VHOST}"
fi


# Redis service bindings
if [ ! -z "$REDIS_HOSTNAME" ]; then
   if [ ! -z "$REDIS_PASSWORD" ]; then
   export SVC_URI_REDIS="${REDIS_PROTOCOL}://${REDIS_PASSWORD}@${REDIS_HOSTNAME}:${REDIS_PORT}"
   else
   export SVC_URI_REDIS="${REDIS_PROTOCOL}://${REDIS_HOSTNAME}:${REDIS_PORT}"
   fi
fi


# Kafka service bindings
if [ ! -z "$KAFKA_HOSTNAME" ]; then
   export SVC_URI_KAFKA="${KAFKA_PROTOCOL}://${KAFKA_USERNAME}:${KAFKA_PASSWORD}@${KAFKA_HOSTNAME}:${KAFKA_PORT}"

fi

node --max-old-space-size=1024 --min-semi-space-size=64 src/start.js
