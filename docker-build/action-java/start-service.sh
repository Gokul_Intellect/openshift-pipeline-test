#!/bin/sh
#set -x

#IFS=",";

for CERT_COMPONENT in ${CERTIFICATES_COMPONENTES[@]}
do
   echo "$CERT_COMPONENT"
   CERTIFICATE=${!CERT_COMPONENT}
     echo "$CERTIFICATE" > ${CERT_COMPONENT}.crt

 #import certificate (trust store) to the JVM of the pod
 keytool -noprompt -import -trustcacerts -keystore "$JAVA_HOME/jre/lib/security/cacerts" -storepass changeit -alias $CERT_COMPONENT -import -file "${CERT_COMPONENT}.crt"
done


# Registry service bindings
if [ ! -z "$REGISTRY_HOSTNAME" ]; then
   export SVC_URI_REGISTRY="${REGISTRY_PROTOCOL}://${REGISTRY_USERNAME}:${REGISTRY_PASSWORD}@${REGISTRY_HOSTNAME}:${REGISTRY_PORT}/eureka/"
   APP_OPTS="$APP_OPTS -Deureka.client.serviceUrl.defaultZone=$SVC_URI_REGISTRY"
fi

# Git service bindings
if [ ! -z "$GIT_HOSTNAME" ]; then
   export SVC_URI_GIT="${GIT_PROTOCOL}://${GIT_USERNAME}:${GIT_PASSWORD}@${GIT_HOSTNAME}:${GIT_PORT}/${GIT_REPOSITORY}"
   APP_OPTS="$APP_OPTS -Dspring.cloud.config.server.git.uri=${GIT_PROTOCOL}://${GIT_HOSTNAME}:${GIT_PORT}/${GIT_REPOSITORY}"
fi

# Elasticsearch service bindings
if [ ! -z "$ELASTICSEARCH_HOSTNAME" ]; then
   if [ ! -z "$ELASTICSEARCH_USERNAME" ]; then
      export SVC_URI_ELASTICSEARCH="${ELASTICSEARCH_PROTOCOL}://${ELASTICSEARCH_USERNAME}:${ELASTICSEARCH_PASSWORD}@${ELASTICSEARCH_HOSTNAME}:${ELASTICSEARCH_PORT}"    
   else
      export SVC_URI_ELASTICSEARCH="${ELASTICSEARCH_PROTOCOL}://${ELASTICSEARCH_HOSTNAME}:${ELASTICSEARCH_PORT}"
   fi

   # For call urrent Action API's , Will be removed later , Helps in minimising rework
   APP_OPTS="$APP_OPTS -DES.DB.User=${ELASTICSEARCH_USERNAME} -DES.DB.Password=${ELASTICSEARCH_PASSWORD} -DES.DB.Host=${ELASTICSEARCH_HOSTNAME} -DES.DB.Port=${ELASTICSEARCH_PORT} -DES.DB.Protocol=${ELASTICSEARCH_PROTOCOL} -DES.enrichment.DB.Protocol=${ELASTICSEARCH_PROTOCOL} -DES.enrichment.DB.Host=${ELASTICSEARCH_HOSTNAME} -DES.enrichment.DB.Port=${ELASTICSEARCH_PORT} -DES.enrichment.DB.User=${ELASTICSEARCH_USERNAME} -DES.enrichment.DB.Pass=${ELASTICSEARCH_PASSWORD} -DES.DB.Pass=${ELASTICSEARCH_PASSWORD}"

fi

# RabbitMQ service bindings
if [ ! -z "$RABBITMQ_HOSTNAME" ]; then
   export SVC_URI_RABBITMQ="${RABBITMQ_PROTOCOL}://${RABBITMQ_USERNAME}:${RABBITMQ_PASSWORD}@${RABBITMQ_HOSTNAME}:${RABBITMQ_PORT}/${RABBITMQ_VHOST}"

   # For call urrent Action API's , Will be removed later , Helps in minimising rework
   APP_OPTS="$APP_OPTS -DDigital.RabbitMQ.Host=${RABBITMQ_HOSTNAME} -DDigital.RabbitMQ.Port=${RABBITMQ_PORT} -DDigital.RabbitMQ.User=${RABBITMQ_USERNAME} -DDigital.RabbitMQ.Password=${RABBITMQ_PASSWORD} -DDigital.RabbitMQ.VHost=${RABBITMQ_VHOST} -DDigital.RabbitMQ.Pass=${RABBITMQ_PASSWORD}"
fi

# Redis service bindings
if [ ! -z "$REDIS_HOSTNAME" ]; then
   if [ ! -z "$REDIS_PASSWORD" ]; then
   export SVC_URI_REDIS="${REDIS_PROTOCOL}://${REDIS_PASSWORD}@${REDIS_HOSTNAME}:${REDIS_PORT}"
   else
   export SVC_URI_REDIS="${REDIS_PROTOCOL}://${REDIS_HOSTNAME}:${REDIS_PORT}"
   fi
   APP_OPTS="$APP_OPTS -DRedis.DB.Url=${REDIS_HOSTNAME}:${REDIS_PORT} -DRedis.DB.Password=$REDIS_PASSWORD -DRedis.DB.Pass=$REDIS_PASSWORD"


fi

# Kafka service bindings
if [ ! -z "$KAFKA_HOSTNAME" ]; then
   export SVC_URI_KAFKA="${KAFKA_PROTOCOL}://${KAFKA_USERNAME}:${KAFKA_PASSWORD}@${KAFKA_HOSTNAME}:${KAFKA_PORT}"

   APP_OPTS="$APP_OPTS -DKafka.BootstrapServers=${KAFKA_HOSTNAME}:${KAFKA_PORT}"
fi
#echo $APP_OPTS
if [ ! -z $KERBEROS_CONFIG_PATH ]; then
  java $JAVA_OPTS $EXTERNAL_JARS $APP_OPTS -Djava.security.egd=file:/dev/./urandom -Djava.security.krb5.conf=${KERBEROS_CONFIG_PATH} -jar /cbx-java-service/cbx-service.jar
elif [ $NEW_RELIC_AGENT_ENABLED ]; then
  java $JAVA_OPTS $APP_OPTS $EXTERNAL_JARS $NEW_RELIC_JARS -Djava.security.egd=file:/dev/./urandom -jar /cbx-java-service/cbx-service.jar
else
  #java $JAVA_OPTS $APP_OPTS -Dloader.path=custom-scripts/ -Djava.security.egd=file:/dev/./urandom -jar /cbx-java-service/cbx-service.jar
  java $JAVA_OPTS $EXTERNAL_JARS $APP_OPTS -Djava.security.egd=file:/dev/./urandom -jar /cbx-java-service/cbx-service.jar
fi

#java $JAVA_OPTS $APP_OPTS -Dspring.profiles.active=dev -Djava.security.egd=file:/dev/./urandom -jar /cbx-java-service/cbx-service.jar
