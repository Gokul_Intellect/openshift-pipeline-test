apiVersion: v1
kind: Template
labels:
  template: java-services-template
message: |-
  The following service(s) have been created in your project: ${PROJECT_NAME}.

      Service: ${SERVICE_NAME} version ${SERVICE_VERSION}

  For more information about using this template, including OpenShift considerations, see SOME_LINK_TO_IGTB_PUBLIC_DOCS.
metadata:
  name: digital-chronos-deployment-template
  annotations:
    description: |-
      Deployment descriptors for external digital-chronos, no persistent storage. For more information about using this template, including OpenShift considerations, see SOME_LINK_TO_IGTB_PUBLIC_DOCS.

      NOTE: Some limitations.

objects:
- apiVersion: v1
  kind: Service
  metadata:
    labels:
      project: ${PROJECT_NAME}
    name: ${SERVICE_NAME}-service
  spec:
    ports:
    - name: http
      port: 80
      protocol: TCP
      targetPort: 8080
    selector:
      name: ${SERVICE_NAME}-deployment
    type: ${SERVICE_TYPE}

- apiVersion: v1
  kind: DeploymentConfig
  metadata:
    labels:
      project: ${PROJECT_NAME}
    name: ${SERVICE_NAME}
  spec:
    replicas: 1
    template:
      metadata:
        labels:
          name: ${SERVICE_NAME}-deployment
      spec:
        containers:
        - env:
          # - name: JAVA_OPTS
          #   valueFrom:
          #     configMapKeyRef:
          #       key: jvm-options
          #       name: ${SERVICE_NAME}-config
          - name: logging.level.
            valueFrom:
              configMapKeyRef:
                key: logging-level
                name: ${SERVICE_NAME}-config
           
          # Registry Service Bindings
          - name: REGISTRY_HOSTNAME
            valueFrom:
              configMapKeyRef:
                key: hostname
                name: registry-service-bindings-config
          - name: REGISTRY_PORT
            valueFrom:
              configMapKeyRef:
                key: port
                name: registry-service-bindings-config
          - name: REGISTRY_PROTOCOL
            valueFrom:
              configMapKeyRef:
                key: protocol
                name: registry-service-bindings-config
          - name: REGISTRY_USERNAME
            valueFrom:
              secretKeyRef:
                key: username
                name: registry-service-bindings-secrets
          - name: REGISTRY_PASSWORD
            valueFrom:
              secretKeyRef:
                key: password
                name: registry-service-bindings-secrets
          
          # Eureka
          - name: eureka.instance.preferIpAddress
            valueFrom:
              configMapKeyRef:
                key: eureka-instance-preferIpAddress 
                name: ${SERVICE_NAME}-config  
          - name: eureka.client.enabled
            valueFrom:
              configMapKeyRef:
                key: eureka-client-enabled
                name: ${SERVICE_NAME}-config
          - name: eureka.client.registerWithEureka
            valueFrom:
              configMapKeyRef:
                key: eureka-client-registerWithEureka
                name: ${SERVICE_NAME}-config                          
          
          # # Elastic Search
          # - name: ES.DB.Host
          #   valueFrom:
          #     configMapKeyRef:
          #       key: hostname
          #       name: elasticsearch-service-bindings-config      
          # - name: ES.DB.Port
          #   valueFrom:
          #     configMapKeyRef:
          #       key: port
          #       name: elasticsearch-service-bindings-config       
          # Elasticsearch Service Bindings
          - name: ELASTICSEARCH_HOSTNAME
            valueFrom:
              configMapKeyRef:
                key: hostname
                name: elasticsearch-service-bindings-config
          - name: ELASTICSEARCH_PORT
            valueFrom:
              configMapKeyRef:
                key: port
                name: elasticsearch-service-bindings-config
          - name: ELASTICSEARCH_PROTOCOL
            valueFrom:
              configMapKeyRef:
                key: protocol
                name: elasticsearch-service-bindings-config
          - name: ELASTICSEARCH_USERNAME
            valueFrom:
              secretKeyRef:
                key: username
                name: elasticsearch-service-bindings-secrets
          - name: ELASTICSEARCH_PASSWORD
            valueFrom:
              secretKeyRef:
                key: password
                name: elasticsearch-service-bindings-secrets 
         
         # Rabbit MQ
          - name: Digital.RabbitMQ.Host
            valueFrom:
              configMapKeyRef:
                key: hostname
                name: rabbitmq-service-bindings-config      
          - name: Digital.RabbitMQ.Port
            valueFrom:
              configMapKeyRef:
                key: port
                name: rabbitmq-service-bindings-config 
          - name: Digital.RabbitMQ.User
            valueFrom:
              secretKeyRef:
                key: username
                name: rabbitmq-service-bindings-secrets      
          - name: Digital.RabbitMQ.Password
            valueFrom:
              secretKeyRef:
                key: password
                name: rabbitmq-service-bindings-secrets          
          - name: Digital.RabbitMQ.VHost
            valueFrom:
              configMapKeyRef:
                key: vhost
                name: rabbitmq-service-bindings-config       
          
                   
          # Spring Application
          - name: spring.application.name
            valueFrom:
              configMapKeyRef:
                key: spring-application-name
                name: ${SERVICE_NAME}-config
                
          - name: spring.cloud.config.discovery.serviceId
            valueFrom:
              configMapKeyRef:
                key: spring-cloud-config-discovery-serviceId 
                name: ${SERVICE_NAME}-config      
          - name: spring.cloud.config.name
            valueFrom:
              configMapKeyRef:
                key: spring-cloud-config-name 
                name: ${SERVICE_NAME}-config 
          - name: spring.cloud.config.profile
            valueFrom:
              configMapKeyRef:
                key: spring-cloud-config-profile 
                name: ${SERVICE_NAME}-config 
          - name: spring.cloud.config.label
            valueFrom:
              configMapKeyRef:
                key: spring-cloud-config-label
                name: ${SERVICE_NAME}-config
          - name: spring.cloud.config.fail-fast
            valueFrom:
              configMapKeyRef:
                key: spring-cloud-config-fail-fast
                name: ${SERVICE_NAME}-config                                   
          - name: security.basic.enabled
            valueFrom:
              configMapKeyRef:
                key: security-basic-enabled
                name: ${SERVICE_NAME}-config
          - name: spring.cloud.config.discovery.enabled
            valueFrom:
              configMapKeyRef:
                key: spring-cloud-config-discovery-enabled 
                name: ${SERVICE_NAME}-config     
          - name : spring.cloud.services.registrationMethod
            valueFrom:
              configMapKeyRef:
                key: spring-cloud-services-registrationMethod
                name: ${SERVICE_NAME}-config
          image: "${DOCKER_REGISTRY}/${SERVICE_NAME}:${SERVICE_VERSION}"
          name: ${SERVICE_NAME}-java-service
          ports:
            - containerPort: 8080
          imagePullPolicy: Always
          resources:
            limits:
              cpu: 500m
              memory: 500Mi
            requests:
              cpu: 300m
              memory: 300Mi
          readinessProbe:
            initialDelaySeconds: 30
            tcpSocket:
              port: 8080
            initialDelaySeconds: 120
            timeoutSeconds: 5
          livenessProbe:
            initialDelaySeconds: 30
            tcpSocket:
              port: 8080
            initialDelaySeconds: 120
            timeoutSeconds: 5
            failureThreshold: 10
        imagePullSecrets:
        - name: jfrog-repo-secret

parameters:
- description: Openshift project to be used.
  displayName: openshift project
  name: PROJECT_NAME
  required: true
  value: "cbx"
- description: Version of SERVICE image to be used (1.0, 1.1, 1.2, or latest).
  displayName: Version of SERVICE Image
  name: SERVICE_VERSION
  required: true
  value: "1.1.3"
- description: Name of service/docker image to be used 
  displayName: Name of service/docker imge 
  name: SERVICE_NAME
  required: true
  value: "digital-chronos"
- description: Docker REGISTRY URI (igtb).
  displayName: Docker REGISTRY URI
  name: DOCKER_REGISTRY
  required: true
  value: "igtb-cbx-docker-dev-local.jfrog.io"
- description: Openshift Service Type (clusterIP , NodePortor).
  displayName: Openshift Service Type
  name: SERVICE_TYPE
  required: true
  value: "NodePort"  



