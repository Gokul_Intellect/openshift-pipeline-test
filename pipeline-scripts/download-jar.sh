#!/bin/sh

#### Download Jar from JFrog or S3 to local

# Check presence of input parameters
if [ "${V_JFrog_Jar_Path}" == "" ] && [ "${V_S3_Rel_Tag}" == "" ]; then
	echo "Either JFrog Jar path or S3 release tag is required!"
	exit 1
fi

if [ "${V_App_Name}" == "" ] && [ "${V_S3_Rel_Tag}" != "" ]; then
	echo "App name is required to download from S3!"
	exit 1
fi


export AWS_SECRET_ACCESS_KEY=$S3_PUBLISH_ACCESS_KEY_SECRET
export AWS_ACCESS_KEY_ID=$S3_PUBLISH_ACCESS_KEY_ID
export AWS_DEFAULT_REGION=us-east-1


if [ "${V_S3_Rel_Tag}" != "" ]; then

   S3_DOWNLOAD_PATH=s3://${S3_PUBLISH_BUCKET}/${V_App_Name}/${V_S3_Rel_Tag}/${V_App_Name}-${V_S3_Rel_Tag}.jar

   echo "S3 copy from ${S3_DOWNLOAD_PATH}"

   aws s3 cp ${S3_DOWNLOAD_PATH} ./${CBX_SERVICE_JAR}

else

   echo "JFrog download from ${V_JFrog_Jar_Path}"

   curl -L -u ${REPO_USER_DEVELOPER}:${REPO_SECRET_DEVELOPER} ${V_JFrog_Jar_Path} --output ./${CBX_SERVICE_JAR}

fi
