#!/bin/sh

#### Build docker image and publish to registry

# Check presence of input parameters
if [ -z "${V_App_Name}" ] || [ -z "${V_Publish_Image_Version}" ]; then
	echo "Both App name and Publish version are required!"
	exit 1
fi


## prepare Jar
CBX_SERVICE_JAR=service.jar
mv ${CBX_SERVICE_JAR} ${BUILD_DIR}/
cd ${BUILD_DIR}
ls -l

## docker login
docker login ${DOCKER_REGISTRY_PERF} -u $JFROG_USERNAME_W -p $JFROG_PASSWORD_W
CBX_SERVICE_NAME=${V_App_Name}
CBX_SERVICE_VERSION=${V_Publish_Image_Version}

# docker build
docker build \
	--build-arg SERVICE_JAR=${CBX_SERVICE_JAR} \
	-t ${DOCKER_REGISTRY_PERF}/${CBX_SERVICE_NAME}:latest \
	-t ${DOCKER_REGISTRY_PERF}/${CBX_SERVICE_NAME}:${CBX_SERVICE_VERSION} \
	-f Dockerfile .

# docker push
docker push ${DOCKER_REGISTRY_PERF}/${CBX_SERVICE_NAME}:latest
docker push ${DOCKER_REGISTRY_PERF}/${CBX_SERVICE_NAME}:${CBX_SERVICE_VERSION}
